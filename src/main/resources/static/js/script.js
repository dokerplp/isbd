function fillLocations(name) {
    $.ajax({
        type: "GET",
        url: "/location/" + name,
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        success: function(o) {
            let select = document.getElementById('locations')
            select.innerHTML = ""
            for (let i = 0; i < o.length; i++) {
                let opt = document.createElement('option');
                opt.value = o[i].id
                opt.text = o[i].name
                select.appendChild(opt);
            }
        }
    });
}

function fillRace() {
    $.ajax({
        type: "GET",
        url: "/races",
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        success: function(o) {
            let select = document.createElement('select');
            for (let i = 0; i < o.length; i++) {
                let opt = document.createElement('option');
                opt.value = o[i].id
                opt.text = o[i].name
                select.appendChild(opt);
            }
            let selects = document.getElementsByClassName('race')
            for (let i = 0; i < o.length; i++) {
                selects[i].innerHTML = select.innerHTML
            }
        }
    });
}

function createConflict() {
    $.ajax({
        type: "POST",
        url: "/conflict",
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        data: JSON.stringify({
            location: document.getElementById('locations').value,
            name: document.getElementById('conflict_name').value,
            desc: document.getElementById('conflict_desc').value,
            date: document.getElementById('conflict_date').value,
            sides: [
                {
                    name: document.getElementById('side_1_name').value,
                    goal: document.getElementById('side_1_goal').value,
                    race: document.getElementById('side_1_race').value,
                    count: document.getElementById('side_1_count').value,
                },
                {
                    name: document.getElementById('side_2_name').value,
                    goal: document.getElementById('side_2_goal').value,
                    race: document.getElementById('side_2_race').value,
                    count: document.getElementById('side_2_count').value,
                }
            ]
        }),
        success: function(o) {
            alert('Conflict successfully created!')
        },
        error: function(o) {
            alert('Error...')
        }
    });
}

function valhalla() {
    $.ajax({
        type: "POST",
        url: "/valhalla",
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        success: function(o) {
            alert(o)
        },
        error: function(o) {
            alert('Error...')
        }
    });
}

function changeWorld(name, src) {
    document.getElementById('world_name').innerHTML = name;
    document.getElementById('world_map').src = src;
    fillLocations(name)
}

window.onload = function () {
    changeWorld("Midgard", "img/worlds/midgard.jpeg");
    fillRace()
}

document.getElementById("start").addEventListener("click", (e) => {
    createConflict();
});

document.getElementById("valhalla").addEventListener("click", (e) => {
    valhalla();
});


document.getElementById("asgard").addEventListener("click", (e) => {
    changeWorld("Asgard", "img/worlds/asgard.jpeg");
});

document.getElementById("jotunheim").addEventListener("click", (e) => {
    changeWorld("Jotunheim", "img/worlds/jotunheim.jpeg");
});

document.getElementById("svartalheim").addEventListener("click", (e) => {
    changeWorld("Svartalheim", "img/worlds/svartalheim.jpeg");
});

document.getElementById("muspelheim").addEventListener("click", (e) => {
    changeWorld("Muspelheim", "img/worlds/muspelheim.jpeg");
});

document.getElementById("helheim").addEventListener("click", (e) => {
    changeWorld("Helheim", "img/worlds/helheim.jpeg");
});

document.getElementById("nilfheim").addEventListener("click", (e) => {
    changeWorld("Nilfheim", "img/worlds/nilfheim.jpeg");
});

document.getElementById("vanaheim").addEventListener("click", (e) => {
    changeWorld("Vanaheim", "img/worlds/vanaheim.jpeg");
});

document.getElementById("alfheim").addEventListener("click", (e) => {
    changeWorld("Alfheim", "img/worlds/alfheim.jpeg");
});

document.getElementById("midgard").addEventListener("click", (e) => {
    changeWorld("Midgard", "img/worlds/midgard.jpeg");
});