package dokerplp.proj.entity

import javax.persistence.*

@Entity
@Table(name = "worlds")
class World (
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "main_race_id", referencedColumnName = "id")
    var race: Race? = null,

    @OneToMany(mappedBy="world", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    var locations: List<Location>? = null
)