package dokerplp.proj.entity

import lombok.NoArgsConstructor
import javax.persistence.*

@Entity
@Table(name = "gods")
data class God (
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    var location: Location? = null,
)