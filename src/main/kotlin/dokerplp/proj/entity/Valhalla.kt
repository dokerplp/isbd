package dokerplp.proj.entity


import javax.persistence.*

@Entity
@Table(name = "valhalla")
class Valhalla (
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "age", nullable = false)
    var age: Long? = null,

    @Column(name = "weight", nullable = false)
    var weight: Long? = null,

    @Column(name = "height", nullable = false)
    var height: Long? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "race_id", referencedColumnName = "id", nullable = false)
    var race: Race? = null
)