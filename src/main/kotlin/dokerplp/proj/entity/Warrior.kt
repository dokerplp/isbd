package dokerplp.proj.entity

import javax.persistence.*

@Entity
@Table(name = "warrior")
class Warrior (
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "age", nullable = false)
    var age: Long? = null,

    @Column(name = "weight", nullable = false)
    var weight: Long? = null,

    @Column(name = "height", nullable = false)
    var height: Long? = null,

    @Column(name = "is_died_in_battle", nullable = false)
    var isDiedInBattle: Boolean? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "favourite_god_id", referencedColumnName = "id")
    var god: God? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "race_id", referencedColumnName = "id", nullable = false)
    var race: Race? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "conflict_side_id", referencedColumnName = "id", nullable = false)
    var conflictSide: ConflictSide? = null
)