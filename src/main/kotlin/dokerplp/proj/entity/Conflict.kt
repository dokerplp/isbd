package dokerplp.proj.entity

import javax.persistence.*

@Entity
@Table(name = "conflicts")
data class Conflict (
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "description", nullable = false)
    var description: String? = null,

    @OneToOne
    var location: Location? = null,

    @JoinColumn(name = "conflict_id")
    @OneToMany
    var sides: List<ConflictSide>? = null
)