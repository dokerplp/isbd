package dokerplp.proj.repository

import dokerplp.proj.entity.World
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface WorldRepository: CrudRepository<World, Long> {
    fun getWorldByNameIgnoreCase(name: String): Optional<World>
}