package dokerplp.proj.repository

import dokerplp.proj.entity.Valhalla
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface ValhallaRepository: JpaRepository<Valhalla, Long> {
    @Transactional
    @Query(nativeQuery = true, value ="select send_warriors_to_valhalla()")
    fun sendWarriorsToValhalla(): Int
}