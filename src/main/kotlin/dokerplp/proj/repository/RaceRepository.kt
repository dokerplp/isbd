package dokerplp.proj.repository

import dokerplp.proj.entity.Race
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RaceRepository: CrudRepository<Race, Long> {

    @Query("select r from Race r")
    fun getAll(): List<Race>
}