package dokerplp.proj.repository

import dokerplp.proj.entity.Conflict
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface ConflictRepository: JpaRepository<Conflict, Long> {

    @Transactional
    @Query(nativeQuery = true, value ="select create_conflict(?1, ?2, ?3, to_date(?4, 'YYYY-MM-DD'), ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)")
    fun createConflict (
        locationId: Int,
        conflictName: String,
        conflictDescription: String,
        conflictDate: String,
        cs1Name: String,
        cs2Name: String,
        cs1Desc: String,
        cs2Desc: String,
        cs1Count: Int,
        cs2Count: Int,
        cs1Race: Int,
        cs2Race: Int
    )
}