package dokerplp.proj.service

import dokerplp.proj.message.ConflictReq
import dokerplp.proj.repository.ConflictRepository
import org.springframework.stereotype.Service
import java.time.format.DateTimeFormatter

@Service
class ConflictService (
    val conflictRepository: ConflictRepository
) {

    fun createConflict(conflictReq: ConflictReq) {
        val date = DateTimeFormatter.ISO_LOCAL_DATE.format(conflictReq.date)
        conflictRepository.createConflict (
            conflictReq.location,
            conflictReq.name,
            conflictReq.desc,
            date,
            conflictReq.sides[0].name,
            conflictReq.sides[1].name,
            conflictReq.sides[0].goal,
            conflictReq.sides[1].goal,
            conflictReq.sides[0].count,
            conflictReq.sides[1].count,
            conflictReq.sides[0].race,
            conflictReq.sides[1].race
        )
    }
}