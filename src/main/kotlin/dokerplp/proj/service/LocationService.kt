package dokerplp.proj.service

import dokerplp.proj.message.LocationResp
import dokerplp.proj.repository.WorldRepository
import org.springframework.stereotype.Service

@Service
class LocationService(
    val worldRepository: WorldRepository
) {
    fun getLocationsByWorldName(worldName: String): List<LocationResp> {
        val world = worldRepository.getWorldByNameIgnoreCase(worldName)
        if (world.isEmpty) return emptyList()
        return world.get().locations!!.stream().map { o -> LocationResp(o) }.toList()
    }
}