package dokerplp.proj.service

import dokerplp.proj.repository.ValhallaRepository
import org.springframework.stereotype.Service

@Service
class ValhallaService (
    val valhallaRepository: ValhallaRepository
) {
    fun sendWarriorsToValhalla(): Int {
        return valhallaRepository.sendWarriorsToValhalla()
    }
}