package dokerplp.proj.service

import dokerplp.proj.entity.Race
import dokerplp.proj.repository.RaceRepository
import org.springframework.stereotype.Service

@Service
class RaceService (
    val raceRepository: RaceRepository
) {
    fun getAllRaces(): List<Race> {
        return raceRepository.getAll();
    }
}