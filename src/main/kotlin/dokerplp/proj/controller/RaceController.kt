package dokerplp.proj.controller

import dokerplp.proj.entity.Race
import dokerplp.proj.service.RaceService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class RaceController (
    val raceService: RaceService
) {

    @GetMapping("/races")
    fun getAllRaces(): List<Race> {
        return raceService.getAllRaces();
    }
}