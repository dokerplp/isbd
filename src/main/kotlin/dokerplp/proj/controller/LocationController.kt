package dokerplp.proj.controller

import dokerplp.proj.entity.Location
import dokerplp.proj.message.LocationResp
import dokerplp.proj.service.LocationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class LocationController(
    @Autowired private val locationService: LocationService
) {

    @CrossOrigin
    @GetMapping("/location/{world}")
    fun locationByWorldName(@PathVariable world: String): List<LocationResp> {
        return locationService.getLocationsByWorldName(world)
    }
}