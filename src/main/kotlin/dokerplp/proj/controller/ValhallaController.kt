package dokerplp.proj.controller

import dokerplp.proj.message.ConflictReq
import dokerplp.proj.service.ValhallaService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ValhallaController (
    val valhallaService: ValhallaService
) {
    @PostMapping("/valhalla")
    fun createConflict(): String {
        val count =  valhallaService.sendWarriorsToValhalla()
        return "Success! $count warriors were taken to valhalla!"
    }
}