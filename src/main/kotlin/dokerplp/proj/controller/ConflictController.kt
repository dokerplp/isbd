package dokerplp.proj.controller

import dokerplp.proj.message.ConflictReq
import dokerplp.proj.service.ConflictService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ConflictController (
    val conflictService: ConflictService
) {

    @PostMapping("/conflict")
    fun createConflict(@RequestBody conflict: ConflictReq) {
        conflictService.createConflict(conflict)
    }
}