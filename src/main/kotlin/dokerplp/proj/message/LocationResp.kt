package dokerplp.proj.message

import dokerplp.proj.entity.Location
import javax.persistence.Column

class LocationResp (
    val id: Long?,
    val name: String?,
    val description: String?,
) {
    constructor(location: Location): this(location.id, location.name, location.description)
}