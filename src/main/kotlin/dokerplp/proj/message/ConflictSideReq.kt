package dokerplp.proj.message

class ConflictSideReq (
    val name: String,
    val goal: String,
    val race: Int,
    val count: Int
)