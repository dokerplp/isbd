package dokerplp.proj.message

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate

class ConflictReq (
    val location: Int,
    val name: String,
    val desc: String,
    @JsonFormat(pattern = "yyyy-MM-dd")
    val date: LocalDate,
    val sides: List<ConflictSideReq>
)