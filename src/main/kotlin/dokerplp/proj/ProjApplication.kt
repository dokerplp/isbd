package dokerplp.proj

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProjApplication

fun main(args: Array<String>) {
    runApplication<ProjApplication>(*args)
}
