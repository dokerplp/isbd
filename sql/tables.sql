create table races
(
    id          int          not null,
    name        varchar(128) not null,
    description varchar(256) not null,
    primary key (id)
);


create table worlds
(
    id           int          not null,
    name         varchar(32)  not null,
    description  varchar(256) not null,
    main_race_id int,
    primary key (id),
    foreign key (main_race_id) references races (id)
);

create table locations
(
    id          int          not null,
    name        varchar(32)  not null,
    description varchar(256) not null,
    world_id    int          not null,
    primary key (id),
    foreign key (world_id) references worlds (id)
);


create table conflicts
(
    id          int          not null,
    location_id int          not null,
    name        varchar(32)  not null,
    description varchar(256) not null,
    date        date         not null,
    primary key (id),
    foreign key (location_id) references locations (id)
);

create table conflict_sides
(
    id          int          not null,
    conflict_id int          not null,
    name        varchar(64)  not null,
    goal        varchar(256) not null,
    primary key (id),
    foreign key (conflict_id) references conflicts (id) on delete cascade
);

create table gods
(
    id          int          not null,
    location_id int          not null,
    name        varchar(32)  not null,
    description varchar(256) not null,
    primary key (id),
    foreign key (location_id) references locations (id)
);

create table warrior
(
    id                int         not null,
    conflict_side_id  int         not null,
    name              varchar(16) not null,
    age               int         not null,
    weight            int         not null,
    height            int         not null,
    is_died_in_battle bool        not null,
    favourite_god_id  int,
    race_id           int         not null,
    primary key (id),
    foreign key (favourite_god_id) references gods (id),
    foreign key (conflict_side_id) references conflict_sides (id) on delete cascade,
    foreign key (race_id) references races (id)
);

create table valkyries
(
    id    int         not null,
    name  varchar(32) not null,
    alias varchar(64) not null,
    primary key (id)
);

create table valkyrie_to_location
(
    valkyrie_id int not null,
    location_id int not null,
    foreign key (valkyrie_id) references valkyries (id),
    foreign key (location_id) references locations (id)
);

create table warrior_names
(
    id   int         not null,
    name varchar(16) not null,
    primary key (id)
);

create table valhalla
(
    id      int         not null,
    name    varchar(16) not null,
    age     int         not null,
    weight  int         not null,
    height  int         not null,
    race_id int         not null,
    primary key (id),
    foreign key (race_id) references races(id)
);

create table valkyries_schedule
(
    id          int  not null,
    valkyrie_id int  not null,
    date        date not null,
    count       int  not null default 0,
    foreign key (valkyrie_id) references valkyries (id),
    primary key (id)
);