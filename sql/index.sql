create index on conflict_sides using hash(conflict_id);

create index on warrior using hash(conflict_side_id);

create index on valkyrie_to_location using hash(location_id);

create index on valkyries_schedule using hash(valkyrie_id);
create index on valkyries_schedule using btree(date);

create index on warrior using btree(age);
create index on warrior using btree(height);
create index on warrior using btree(weight);
create index on warrior using btree(is_died_in_battle);
CREATE EXTENSION pg_trgm;
create index on warrior using gin(name gin_trgm_ops);
create index on warrior using hash(favourite_god_id);