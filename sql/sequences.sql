create sequence warrior_id start 1;
create sequence conflict_id start 1;
create sequence conflict_side_id start 1;
create sequence valhalla_id start 1;
create sequence valkyries_schedule_id start 1;