create or replace function rand_range(a int, b int)
    returns int
    language plpgsql
as
$$
begin
    return (SELECT floor(random() * (b - a) + a)::int);
end;
$$;

create or replace function create_warrior(cs_id int, r_id int)
    returns void
    language plpgsql
as
$$
declare
    n varchar(16) = '';
    a int         = 0;
    w int         = 0;
    h int         = 0;
    i bool        = false;
    g int         = null;
begin
    n := (select name from warrior_names order by random() limit 1);
    a := rand_range(14, 90);
    w := rand_range(60, 210);
    h := rand_range(130, 210);
    if rand_range(1, 10) <= 7 then
        i := true;
    else
        i := false;
    end if;
    if rand_range(1, 10) <= 5 then
        g := (select id from gods order by random() limit 1);
    end if;
    insert into warrior
    values ((SELECT nextval('warrior_id')), cs_id, n, a, w, h, i, g, r_id);
    return;
end;
$$;


create or replace function create_warriors(count int, cs_id int, r_id int)
    returns void
    language plpgsql
as
$$
begin
    for i in 1 .. count
        loop
            perform create_warrior(cs_id, r_id);
        end loop;
end;
$$;

create or replace function create_conflict(
    loc_id int,
    c_name varchar(32),
    c_desc varchar(256),
    c_date date,
    cs1_name varchar(64),
    cs2_name varchar(64),
    cs1_desc varchar(256),
    cs2_desc varchar(256),
    cs1_count int,
    cs2_count int,
    cs1_race int,
    cs2_race int
)
    returns int
    language plpgsql
as
$$
declare
    c_id   int = (select nextval('conflict_id'));
    cs1_id int = (select nextval('conflict_side_id'));
    cs2_id int = (select nextval('conflict_side_id'));
begin
    insert into conflicts values (c_id, loc_id, c_name, c_desc, c_date);
    insert into conflict_sides values (cs1_id, c_id, cs1_name, cs1_desc);
    insert into conflict_sides values (cs2_id, c_id, cs2_name, cs2_desc);
    perform create_warriors(cs1_count, cs1_id, cs1_race);
    perform create_warriors(cs2_count, cs2_id, cs2_race);
    return c_id;
end;
$$;


create or replace function send_valkyrie(con_id int)
    returns bool
    language plpgsql
as
$$
declare
    t_con    conflicts%rowtype;
    s1       int;
    s2       int;
    s1_count int;
    s2_count int;
    s1_race  int;
    s2_race  int;
begin
    select * into t_con from conflicts where id = con_id;
    s1 := (select id from conflict_sides where conflict_id = t_con.id limit 1);
    s2 := (select id from conflict_sides where conflict_id = t_con.id offset 1 limit 1);
    s1_count := (select count(*) from warrior where conflict_side_id = s1);
    s2_count := (select count(*) from warrior where conflict_side_id = s2);
    s1_race := (select race_id from warrior where conflict_side_id = s1 limit 1);
    s2_race := (select race_id from warrior where conflict_side_id = s2 limit 1);
    return not (s1_count + s2_count < 100 or
                s1_count > s2_count * 2 or s2_count > s1_count * 2 or
                s1_race = s2_race or
                (select date_part('day', t_con.date))::integer % 2 = 0);
end;
$$;

create or replace function take_warrior_to_valhalla(con_id int, war_id int)
    returns void
    language plpgsql
as
$$
declare
    t_con  conflicts%rowtype;
    t_war  warrior%rowtype;
    t_valk valkyries%rowtype;
begin
    select * into t_con from conflicts where id = con_id;
    select * into t_war from warrior where id = war_id;
    for t_valk in select *
                  from valkyries
                  where id in (select valkyrie_id from valkyrie_to_location where location_id = t_con.location_id)
        loop
            if (select count(*) from valkyries_schedule where valkyrie_id = t_valk.id and date = t_con.date) = 0 then
                insert into valkyries_schedule
                values ((select nextval('valkyries_schedule_id')), t_valk.id, t_con.date, 0);
            end if;
            if (select count from valkyries_schedule where valkyrie_id = t_valk.id and date = t_con.date) < 100 then
                update valkyries_schedule set count = count + 1 where valkyrie_id = t_valk.id and date = t_con.date;
                insert into valhalla
                values ((select nextval('valhalla_id')), t_war.name, t_war.age, t_war.weight, t_war.height,
                        t_war.race_id);
                return;
            end if;
        end loop;
end;
$$;

create or replace function send_warriors_to_valhalla()
    returns int
    language plpgsql
as
$$
declare
    t_con conflicts%rowtype;
    t_war warrior%rowtype;
    st int;
begin
    st := (select count(*) from valhalla);
    for t_con in select * from conflicts
        loop
            if (select send_valkyrie(t_con.id)) then
                for t_war in select *
                             from warrior
                             where conflict_side_id in (select id from conflict_sides where conflict_id = t_con.id) and
                                     age <= 50 and
                                     age >= 20 and
                                     height >= 180 and
                                     weight <= 120 and
                                     is_died_in_battle and
                                     name not like ('A%') and
                                     name not like ('%n') and
                                     favourite_god_id != (select id from gods where name = 'Thor')
                    loop
                        perform take_warrior_to_valhalla(t_con.id, t_war.id);
                    end loop;
            end if;
            delete from conflicts where id = t_con.id;
        end loop;
    return (select count(*) from valhalla) - st;
end;
$$;
